hakkes-ssh:
	ssh root@85.143.172.178 

syslog-ng:
	ansible-playbook syslog-ng/syslog-ng.yaml

db-backup:
	ansible-playbook db/db-backup.yaml

upload-backup:
	ansible-playbook db/upload-backup.yaml

restart-php-consumer:
	ansible-playbook consumer/restart.yaml

.PHONY: syslog-ng hakkes-ssh db-backup restart-php-consumer
