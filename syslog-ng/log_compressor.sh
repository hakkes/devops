#!/bin/bash
#
directory=$(dirname "$0")
today=$(date +%Y-%m-%d)

for subdir in "$directory"/*
do
  if [[ -d "$subdir" ]]; then
    for dir in "$subdir"/*
    do
      dirName=$(basename "$dir")

      if [[ -d "$dir" && "$dirName" != "$today" && "$dirName" =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]
      then
        tar -czf "$dir.tar.gz" "$dir"
        rm -rf "$dir"
      fi
    done
  fi
done

