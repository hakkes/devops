echo "--------------------------"
folder=$(date '+%Y-%m')
filename="$(find /var/backups -name 'all-database-*' -type f -printf '%T@ %f\n' | sort -n | tail -1 | awk '{print $2}')"

echo "Uploading  db_backup for date: $(date)."

accept='Accept: application/json'
auth="Authorization: OAuth ${ya_disk_token}"

echo "  Creating / checking the folder ${folder} exist on yadisk"
url="https://cloud-api.yandex.net/v1/disk/resources?path=app:/${folder}"
response=$(curl --silent -X GET -H "$accept" -H "$auth" -w "\n%{http_code}\n%{response_body}\n" "$url")

response_body=$(echo "$response" | tail -n 2 | head -n 1)
status_code=$(echo "$response" | tail -n 1)


if [[ $status_code -ne 200 ]] && [[ $status_code -ne 201 ]] && [[ $status_code -ne 409 ]]; then
  echo "  Error: folder creation failed with status code $status_code"
  echo "  Response body: $response_body"
  echo "  --------------------------"
  exit 1
fi
echo "  folder checked / created."
echo "    inquiring an upload link..."

url="https://cloud-api.yandex.net/v1/disk/resources/upload?path=app:/${folder}/${filename}"

response=$(curl --silent -X GET -H "$accept" -H "$auth" -w "\n%{http_code}\n%{response_body}\n" "$url")

response_body=$(echo "$response" | tail -n 2 | head -n 1)
status_code=$(echo "$response" | tail -n 1)

if [[ $status_code -gt 204 ]]; then
  echo "    Error: Request failed with status code $status_code"
  echo "    Response body: $response_body"
  echo "--------------------------"
  exit 1
fi

echo "      upload link_received. uploading file...$(filename)"

href=$(echo "$response_body" | jq -r '.href')

response_upload=$(curl -X POST -F "file=@/var/backups/$filename" -w "\n%{http_code}\n%{response_body}\n" "$href")

response_body_upload=$(echo "$response_upload" | tail -n 2 | head -n 1)
status_code_upload=$(echo "$response_upload" | tail -n 1)

if [[ $status_code_upload -gt 205 ]]; then
  echo "    Error: Request failed with status code $status_code_upload"
  echo "    Response Body: $response_body_upload"
  echo "--------------------------"
  exit 1
fi
echo "      Status code for uploaded file: $status_code_upload"
echo "--------------------------"