## Name
Hakkes devops.

## Description
Contains settings and tools for Hakkes infrastructure. 

## Installation
### MacOS
In case you don't have it, install homebrew:
- `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
Install ansible
- `brew install ansible esolitos/ipa/sshpass`

### Ubuntu
- `sudo apt update && sudo apt install ansible sshpass -y`

### Configuration
- You need to create `vault_password.txt` file in the root directory.
- Insert password for ansible vault (can be got from CEO)
- See `Makefile` for available commands

## Usage
- `make syslog-ng`
Install syslog-ng into Hakkes' host and set configuration up for sending logs into [sematext.com](sematext.com)

- `make db-backup`
Copy a bash script to a remote server. The script does a backup of a database, zips it and also remove the backups older than 30 days. Also, add the script into cron schedule.

- `make upload-backup`
Copy the upload.sh script to a remote server. The script uploads the most recent database zip backup file to the yandex disk. The task is added to cron schedule.

- `make hakkes-ssh`
Root SSH access to remote server. You will have to enter password. If you'd like not to use password every time. You can generate a pair of keys using `ssh-keygen` command. After generation you can copy the keys using following command:
`ssh-copy-id -i ~/.ssh/<name of you private key> <user@host>`
